# ArgoCD

## ArgoCD Installation

To install ArgoCD on Kubernetes, follow these steps:

1. Make sure you have the `kubectl` command-line tool installed and configured to connect to your Kubernetes cluster.

2. Create the `argocd` namespace by running the following command:

```bash
kubectl create namespace argocd

Apply the ArgoCD manifest file to install ArgoCD in the argocd namespace by running the following command:

kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

Check the status of the argocd-server service to see if it has an external IP assigned to it:

kubectl get svc -n argocd argocd-server

If an external IP is assigned, you can proceed to the next step. If there is no external IP, it might take a few moments for the external IP to be provisioned. You can check again later.
Once the external IP is available, open your web browser and enter the following URL, replacing <EXTERNAL-IP> with the external IP address obtained from the previous step:

http://<EXTERNAL-IP>

If you are running Kubernetes locally using Docker Desktop or Minikube, you can use port forwarding to access the service. For example, with Docker Desktop, you can run the following command:

kubectl port-forward svc/argocd-server -n argocd 8080:443

This will forward port 443 of the ArgoCD service to port 8080 on your local machine. Then, you can access the ArgoCD UI by opening https://localhost:8080 in your web browser.

Now yoy can access the ArgoCD UI by opening https://localhost:8080 in your web browser.

When you access the ArgoCD UI, you can use the following credentials to log in:

Username: 
Password: 

Get the password from the new terminal using this command: kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath='{.data.password}' | base64 -d

After logging in, you will have access to the ArgoCD dashboard, where you can manage your applications and perform various GitOps-related tasks.


Suggestions for README

Provide a self-explaining name for your project.
Describe what your project can do and add any relevant references or links.
Consider using badges to convey metadata or project status.
Include visuals such as screenshots or GIFs to demonstrate your project.
Provide installation instructions to help users get started quickly.
Use examples to show usage and expected output.
Mention where users can go for help or support.
Consider including a roadmap for future releases.
Specify if you are open to contributions and any requirements for contributing.
Show appreciation to those who have contributed to the project.
Include licensing information for open source projects.
Indicate the current status of the project.
If development has slowed down or stopped, mention it explicitly and encourage potential maintainers.


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/sela-1090/students/marcuskaganov/infrastructure/argocd.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/sela-1090/students/marcuskaganov/infrastructure/argocd/-/settings/integrations)#

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
